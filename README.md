# 微信小程序--客服组件

#### 介绍
基于微信小程序，实现的客服组件。目前实现文字聊天，图片聊天功能。
敬请期待各位大佬的意见，评论。



#### 安装教程

1. 下载克隆本项目到本地
2. 将customer文件夹，放到你自己的自定义组件的文件夹下 （没有就建一个）
3. 把app.wxss里的样式补充到你自己的app.wxss内（要注意有没有样式冲突）
4. 接下来就可以正常使用组件了



#### 列子
首先在json文件中引入组件，代码如下：
```json
    "usingComponents": {
        "m-chat": "../../../component/customer/customer"
      }
```
在页面中直接使用：
```html
<m-chat id="chat" list="{{list}}" img="{{userInfo2.AvatarUrl}}" isCustomer="true" 
bind:send="sendMsg" bind:upload="uploadImg" bind:tohome="toHome">
</m-chat>
```

这是后台给我的数据结构，你需要根据你拿到的数据结构进行调整

![Image text](https://gitee.com/zzhsun/CustomerService/raw/master/ImgForShow/20190103145535.png)


最终效果

![Image text](https://gitee.com/zzhsun/CustomerService/raw/master/ImgForShow/20190103144537.png)


